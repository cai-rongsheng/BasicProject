from django.db import models


# Create your models here.


# 创建和数据库关联的类
class Student(models.Model):
    gender_choices = (('男', '男'), ('女', '女'))
    sno = models.IntegerField(db_column="SNo", primary_key=True, null=False)  # 学号，主键，不能为空
    name = models.CharField(db_column="SName", max_length=100, null=False)  # 姓名，不能为空，最大100字节
    gender = models.CharField(db_column='Gender', max_length=100, choices=gender_choices)  # 性别
    birthday = models.DateField(db_column='Birthday', null=False)  # 出生日期
    mobile = models.CharField(db_column="Mobile", max_length=100, null=False)  # 手机号码
    email = models.CharField(db_column="Email", max_length=100, null=False)  # 邮箱地址
    address = models.CharField(db_column="Address", max_length=100, null=False)  # 家庭住址

    class Meta:
        managed = True  # 是否会同步到数据库中的表！
        db_table = "Student"  # 默认名称:appname_classname

    # __Str__
    def __str__(self):
        return "学号：%s \t 姓名：%s" % (self.sno, self.name)
