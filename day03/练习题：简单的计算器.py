# 课后习题：简单的计算机


def input_int():
    """输入一个整数"""
    while True:
        # 提醒输入
        num_str = input("请输入一个整数：")
        # 异常处理
        try:
            num = int(num_str)
            return num
        except:
            print("输入的不符合要求")


def input_action():
    """输入一个运算符"""
    while True:
        # 提醒选择
        action = input("请选择要执行的操作（+，-，*，/，%）：")
        # 是否符合要求
        if action not in ['+', '-', '*', '/', '%']:
            print("选择运算符不符合要求")
        else:
            return action.strip()


if __name__ == '__main__':
    # 输入一个整数
    num01 = input_int()
    # 选择运算符
    action = input_action()
    # 输入一个整数
    num02 = input_int()
    # 定义一个变量
    result = 0
    error_flag = False
    # 开始运算
    if action == '+':
        result = num01 + num02
    elif action == "-":
        result = num01 - num02
    elif action == "*":
        result = num01 * num02
    elif action == '/':
        if num02 == 0:
            print("除数不能等于0")
            error_flag = True
        else:
            result = num01 / num02
    elif action == "%":
        result = num01 % num02

    # 输出
    if not error_flag:
        print("结果：%d %s %d = %.2f" % (num01, action, num02, result))
