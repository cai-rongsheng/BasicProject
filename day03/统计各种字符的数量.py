"""
统计各种字符的数量
str = 'fdsafdsaYYYYdsafd43243243!@^%$#复读机啊发动机范德萨就开了发的撒娇'
大写字母：10个 小写字母：12个  数字：18个
汉字：15     其他字符： 18

思路：需要第字符串中每一个字符进行判断 ， 遍历

重要：在写条件选择的时候，如果条件有互斥性，建议写成if --elif ---elif ---else
"""

if __name__ == '__main__':
    str = "fdsafdsaYYYYdsafdRRR43243243!@^%$#复读机啊发动机范德萨就开了发fdafdsa的撒娇"
    # 定义变量
    # dict  ---key_value
    num = {'upper': 0, 'lower': 0, 'number':0,  'chinese': 0, 'other': 0}  # json
    # 遍历每一个字符
    for char in str:
        # 判断大写字母
        if char.isupper():
            num['upper'] += 1
        # 判断小写字母
        elif char.islower():
            num['lower'] += 1
        # 判断是否是数字
        elif char.isdigit():
            num['number'] += 1
        # 判断是不是汉字：
        elif char >= '\u4E00' and char <= '\u9FA5':
            num['chinese'] += 1
        # 其他
        else:
            num['other'] += 1

    print("大写字母：%d\n小写字母：%d\n数字字符：%d\n汉字：%d\n其他字符：%d" %(num['upper'],
                                                        num['lower'],num['number'], num['chinese'],
                                                        num['other']))

"""
写法01：
if char.upper():
if char.lower():
if
if
if
写法02：
if 
elif
elif
elif 
else

汉字 --> unicode 编码 ---》 汉字区间
"""
