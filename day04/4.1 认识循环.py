# 需求： 计算从1+2....+100之和

"""
第1个数：1
第2个数：2
第3个数：3
第4个数： 4
....
后面一个数比前面一个数 +1

"""

if __name__ == '__main__':
    # 定义一个数，是1
    num = 1
    # 定义一个变量 , 记录相加后的结果
    sum = 0
    # 启用循环-- while
    while num <= 100:  # 循环的条件，如果条件是True ,继续循环，如果是False, 循环结束
        # 把数据的累加记录
        sum = sum + num
        # 把num + 1
        num = num + 1
    # 打印结果
    print("和为：", sum)
