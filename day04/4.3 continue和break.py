"""
continue：结束本次循环
break:跳出整个循环
"""

def run_continue(num:int):
    """
    continue函数
    :param num:
    :return:
    """
    i = 0
    while i<num:
        i += 1
        if i == 5:
            continue
        print(i)
   # 结果：1 2 3 4 6 7 8 9 10

def run_break(num: int):
    """
    continue函数
    :param num:
    :return:
    """
    i = 0
    while i < num:
        i += 1
        if i == 5: break
        print(i)
    # 结果：1 2 3 4



if __name__ == '__main__':
    # 调用continue函数
    run_continue(10)
    print("=" * 100)
    # 调用break函数
    run_break(10)