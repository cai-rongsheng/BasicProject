# 输入一个数字，判断是不是质数
# 质数 又叫素数
# 比如 7 ，只能被1 或者 自己整除
# 比如 21 不是素数，1，21， 3，7
import math

def is_prime_number(num: int):
    """
    判断输入的是不是素数
    :param num: 要判断的整数
    :return: 返回的结果
    """
    # 开始循环
    for i in range(2, int(math.sqrt(num))+1):
        # 判断是否能整除
        if num % i == 0:
            return False
    # 从2 --- num-1 都没有能整除，说明是素数
    return True


if __name__ == '__main__':
    num = int(input("输入一个大于1的整数："))
    is_prime = is_prime_number(num)
    print(is_prime)

# 如果输入的是100，  此时要判断 98次
#  2 --- 这个数的平方根