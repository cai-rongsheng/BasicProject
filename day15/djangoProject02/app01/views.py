from django.shortcuts import render
from django.http import HttpResponse


# Create your views here.
# HTTP：
# ：1.  Client --> Server: HttpRequest
# ：2.  Server --> Client: HttpResponse 

music_list= [
    {'id': '120098', 'name':'爱你一万年','author':'刘德华'},
    {'id': '169423','name':'演员','author': '薛之谦'}
]


def index(request):
    """返回给用户一个文本：Hello,Django!"""
    return HttpResponse("Hello,Django!")


def tv(request):
    """返回tv的页面"""
    return render(request, 'tv.html')


def music(request):
    """返回tv的页面"""
    return render(request, 'music.html', {'musics': music_list})