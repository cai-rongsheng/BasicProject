# import Until.module01  # 如果使用 Import关键字导入模块，在调用的时候，需要调用完整的路径


# 使用from --- import  ---
from Until.module01 import RandomItem, Round


if __name__ == '__main__':
    # 获取10个随机值
    nums = RandomItem(50,100,20)
    print(nums.numbers)
    # 想计算圆的周长和面积
    my_round = Round(123.456)
    print("周长为：%.2f 面积为：%.2f" %(my_round.perimeter, my_round.area))