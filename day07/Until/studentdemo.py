"""
本类是为了生成学生成绩的测试数据：
随机生成一个班50个学生的信息，包含的信息有：学号，姓名，性别，班级，成绩明细
具体明细：
- 学号： 95001 ..... 95050
- 姓名：随机生成50个性别
- 性别：随机填充 男和女 
- 班级：填写固定的（初三(4)班）
- 成绩明细：有5门课，分别为（语文、数学、英语、物理、化学），分数随机50-100间

# =====================  需求01：==================
student_result = [
    {'sno': 95001, 'name': '张译丹', 'gender': '男', 'result': [67, 89, 59, 95, 86]},
]

# =====================  需求02：==================

student_result  = {
95001:{
	 'name': '张译丹',
     'gender': '男',
     'class':'初三(4)班',
     'result': [67, 89, 59, 95, 86]
	}
}
# =====================  需求03：==================
student_result  = {
95001:{
	 'name': '张译丹',
     'gender': '男',
     'class':'初三(4)班',
     'result': {'chinese': 67, 'masths': 89, 'english': 59, 'Physics':95, 'Chemistry':86}
	},
95002:{
	 'name': '李丽',
     'gender': '女',
     'class':'初三(4)班',
     'result': {'chinese': 89, 'masths':65, 'english': 71, 'Physics':95, 'Chemistry':68}
	},
}
"""
import random

FIRST_NAME = "赵钱孙李周吴郑王冯陈褚卫蒋沈韩杨朱秦尤许何吕施张孔曹严华金魏陶姜戚" \
             "谢邹喻柏水窦章云苏潘葛奚范彭郎鲁韦昌马苗凤花方俞任袁柳酆鲍史唐费廉岑薛雷贺倪汤"
LAST_NAME = ["奕铭", "瑞霖", "悦竹", "明哲", "铭威", "天翊", "书玮", "凯瑞", "凯丞",
             "健雄", "涵煦", "耀杰", "品鸿", "子毅", "潇然", "子涵", "越彬", "尚芃",
             "冠智", "钰轩", "智辉", "致远", "俊驰", "雨泽", "砚名", "烨磊", "晟睿",
             "文昊", "修洁", "青杉", "远航", "熙栋", "旭尧", "鸿涛", "伟祺", "荣轩",
             "展鸣", "越泽", "浩宇", "瑾瑜", "皓轩", "擎苍", "擎宇", "志泽", "珺博",
             "子轩", "睿渊", "弘文", "哲瀚", "雨泽", "楷瑞", "梓航", "建辉", "晋鹏", "天磊", "绍辉", "泽洋"]
COURSE = ['chinese', 'maths', 'english', 'physics', 'chemistry']

GENDER = ['男', '女']

CLASS = "初三(4)班"


class StudentInfo:

    def __init__(self, student_number: int=50, start=50, end=100):
        self.student_number = student_number
        self.start = start
        self.end = end
        # 存储生成的姓名 （不能重复）
        self.names = []
        # 存储最终的结果
        self.results01 = []  # 格式01
        self.results02 = {}  # 格式02
        self.results03 = {}  # 格式03
        self.results04 = []  # [[],[],[],]

        # 自动生成姓名
        self.get_names()

    def get_random_numbers(self):
        """生成一定数量的随机值"""
        # 定义一个list集合
        temp_result = []
        # 循环
        while True:
            # 生成一个随机数
            temp_number = random.randint(self.start, self.end)
            # 判断是否存在
            if temp_number not in temp_result:
                # 附加
                temp_result.append(temp_number)
                # 判断数量是否够
                if len(COURSE) == len(temp_result):
                    # 返回
                    return temp_result

    def get_names(self):
        """获取一定数量的姓名"""
        # 开始循环
        while True:
            # 生成一个姓
            temp_first_name = FIRST_NAME[random.randint(0, len(FIRST_NAME) - 1)]
            # 生成一个名
            temp_last_name = LAST_NAME[random.randint(0, len(LAST_NAME) - 1)]
            # 判断
            if (temp_first_name + temp_last_name).strip() not in self.names:
                # 附加
                self.names.append(temp_first_name + temp_last_name)
                # 判断数量有没有到
                if len(self.names) == self.student_number:
                    break

    def get_sno(self, sno: str):
        """根据索引返回一个学号"""
        if len(sno) == 1:
            return "9500" + sno
        elif len(sno) == 2:
            return "950" + sno
        elif len(sno) == 3:
            return "95" + sno

    def get_student_result_one(self):
        """生成学生成绩--格式01"""
        for index, value in enumerate(self.names):
            # 定义一个临时的dict
            temp_dict = {}
            # 添加学号
            temp_dict['sno'] = self.get_sno(str(index + 1))
            # 添加姓名
            temp_dict['name'] = value
            # 添加性别
            temp_dict['gender'] = GENDER[random.randint(0, len(GENDER) - 1)]
            # 添加班级
            temp_dict['class'] = CLASS
            # 添加分数
            temp_dict['result'] = self.get_random_numbers()
            # 附加到集合中
            self.results01.append(temp_dict)

    def get_student_result_two(self):
        """生成学生成绩--格式02"""
        # 开始遍历
        for index, value in enumerate(self.names):
            # 拼接value : {'name': '冯皓轩', 'gender': '女', 'class': '初三(4)班', 'result': [83, 66, 60, 93, 69]}
            # 定义一个临时的dict
            temp_dict = {}
            # 添加姓名
            temp_dict['name'] = value
            # 添加性别
            temp_dict['gender'] = GENDER[random.randint(0, len(GENDER) - 1)]
            # 添加班级
            temp_dict['class'] = CLASS
            # 添加分数
            temp_dict['result'] = self.get_random_numbers()
            # 附加到字典集合
            self.results02[self.get_sno(str(index + 1))] = temp_dict

    def get_student_result_three(self):
        """生成学生成绩--格式03"""
        for index, value in enumerate(self.names):
            # 拼接value : {'name': '冯皓轩', 'gender': '女', 'class': '初三(4)班', 'result': [83, 66, 60, 93, 69]}
            # 定义一个临时的dict
            temp_dict = {}
            # 添加姓名
            temp_dict['name'] = value
            # 添加性别
            temp_dict['gender'] = GENDER[random.randint(0, len(GENDER) - 1)]
            # 添加班级
            temp_dict['class'] = CLASS
            # 添加分数
            # 定义一个临时分数字典
            temp_result_dict = {}
            results = self.get_random_numbers()
            for i, v in enumerate(results):
                temp_result_dict[COURSE[i]] = v
            # 附加到集合
            temp_dict['result'] = temp_result_dict
            # 附加到字典集合
            self.results03[self.get_sno(str(index + 1))] = temp_dict

    def get_student_result_four(self):
        # 开始循环
        for index, value in enumerate(self.names):
            # 临时定义一个list集合
            temp_list = []
            # 添加学号
            temp_list.append(self.get_sno(str(index + 1)))
            # 添加姓名
            temp_list.append(value)
            # 添加性别
            temp_list.append(GENDER[random.randint(0, len(GENDER) - 1)])
            # 添加班级
            temp_list.append(CLASS)
            # 添加成绩
            temp_results = self.get_random_numbers()
            for one_result in temp_results:
                temp_list.append(one_result)
            # 附加到外层数据库
            self.results04.append(temp_list)
