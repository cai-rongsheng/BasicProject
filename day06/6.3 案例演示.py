"""
随机生成一个班10个学生的信息，包含的信息有：学号，姓名，性别，班级，成绩明细
具体明细：
- 学号： 95001 ..... 95050
- 姓名：随机生成50个姓名
- 性别：随机填充 男和女
- 班级：填写固定的（初三(4)班）
- 成绩明细：有5门课，分别为（语文、数学、英语、物理、化学），分数随机50-100间
==============================================================================
存储格式01：
# =====================  需求01：==================
student_result = [
    {'sno': 95001, 'name': '张译丹', 'gender': '男', 'result': [67, 89, 59, 95, 86]},
    {'sno': 95002, 'name': '马丽', 'gender': '女', 'result': [67, 89, 59, 95, 86]},

]

"""
import random

NAMES = ['张译丹', '马丽', '王进', '陈鹏', '马小明', "秦始皇", '朱剑锋', "王小平", '王小坡', '陈丽丽']
GENDER = ['男', '女']
CLASS = '初三(4)班'
INFOS = ['sno', 'name', 'gender', 'class', 'result']


def get_random_numbers(start, end, number):
    """
    获取某一个范围内指定数量的随机值
    :param start: 范围的开始值
    :param end: 范围的结束值
    :param number: 数量
    :return: 返回list集合
    """
    # 定义一个List集合存储分数
    results = set()
    # 开始循环
    while len(results) < number:
        # 生成一个随机值
        results.add(random.randint(start, end))
    # 返回
    return list(results)


def get_one_student_result(order: int, name: str):
    """
    生成一个学生的成绩明细
    :return:
    """
    # 根据list初始化一个dict
    one_student_dict = {}.fromkeys(INFOS)

    # 添加学号
    if len(str(order)) == 1:
        one_student_dict['sno'] = '9500' + str(order)
    elif len(str(order)) == 2:
        one_student_dict['sno'] = '950' + str(order)
    elif len(str(order)) == 3:
        one_student_dict['sno'] = '95' + str(order)
    # 添加姓名
    one_student_dict['name'] = name
    # 添加性别
    one_student_dict['gender'] = GENDER[random.randint(0, len(GENDER) - 1)]
    # 添加班级
    one_student_dict['class'] = '初三(4)班'
    # 添加成绩
    one_student_dict['result'] = get_random_numbers(50, 100, 5)

    # 返回
    return one_student_dict


def get_class_student():
    """获得一个班级的成绩"""
    # 定义一个List存储所有的学生
    all_students_result = []
    # 遍历
    for index, val in enumerate(NAMES):
        all_students_result.append(get_one_student_result(index + 1, val))

    # 返回
    return all_students_result


if __name__ == '__main__':
    results = get_class_student()
    # 输出
    for one in results:
        print(one)
