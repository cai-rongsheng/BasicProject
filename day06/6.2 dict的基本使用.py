# 定义字典
dict01 = {'sno': 95001, 'name': '陈军', 'gender': '男','brithday': '1990-10-10'}
# 遍历的方法01：for --in--(默认就是遍历keys())
for key in dict01:
    print(dict01[key])

# 遍历的方法02：for --in-- (和第一种是等价的)
for key in dict01.keys():
    print(key, dict01[key])

# 遍历的方法03：for ---in---
for (key, val) in dict01.items():
    print(key, val)

# 只要遍历值
for val in dict01.values():
    print(val)