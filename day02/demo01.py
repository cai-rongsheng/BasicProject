"""
需求：输入圆的半径，计算圆的周长和面积，结果要保留两位小数，对圆的半径的输入进行有效性校验
"""
"""
写代码的规范
1. 代码要以模块的方式呈现，模块的最小单位为函数！
2. python中变量的命令
   1) 变量的命令在Python中推荐用小写字母， 或者多个小写字母通过_连接 
      学生的总人数： java : studentTotalNumber
                   Python: student_total_number
     最无脑的命名法： 周长：num01   面积:num02 ------ num50 
     最low逼得命名法： 周长-->zhouchang   面积--> mianji 
     
   2) 针对函数的命令，一般采用“动词_名词”格式
                   
3. 写好注释 -- 解释代码
    单行 - # 
    多行 - 三个双引号，或者三个单引号
"""

# 全局的变量
PI = 3.1415926


def input_num():
    """
    输入一个有效的数字
    :return: 返回输入有效的数字
    """
    # 使用循环来实现，如果输入的无效，就提示重新输入
    while True:
        # 提醒输入
        radii_str = input("请输入圆的半径：")
        # 使用异常处理
        try:
            radii = float(radii_str)
            # 返回
            return radii
        except:
            print("输入的半径无效")


def get_area(radii: float):
    """
    根据半径计算圆的面积
    :param radii: 提供半径的值
    :return: 返回计算好的面积值
    """
    # 返回面积的值
    return PI * radii * radii


def get_perimeter(radii: float):
    """
    根据半径计算圆的周长
    :param radii: 提供的半径的值
    :return: 返回计算好的周长
    """
    # 返回周长的值
    return 2 * PI * radii


# main函数： 程序的入口
if __name__ == '__main__':
    # 调用函数，提醒输入半径，返回一个符合要求的值
    radii = input_num()
    # 输出
    print("圆的周长为: %.2f" % get_perimeter(radii))
    print("圆的面积为：%.2f" % get_area(radii))


