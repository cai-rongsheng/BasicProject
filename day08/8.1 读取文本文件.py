

def read_file_one(path: str):
    """
    读取文件，存储的格式为:[[],[],[],[],]
    :param path: 文件的路径
    :return: 返回list集合
    """
    # 定义一个集合
    students = []
    # 开始读取
    try:
        # 使用with打开文件
        with open(path, mode="r", encoding="GBK") as fd:
            # 读取一行文件
            one_line = fd.readline()  # 一次读取一行，识别到\n  \n\t就结束
            # 判断有没有数据
            while one_line:
                # 处理读取的内容  # 95004,张丽,男,1995/3/5,13482034096,zhangli@iLync.cn,上海市徐汇区习勤路100弄
                one_line_list = one_line.strip().split(",")
                # 附加到students中
                students.append(one_line_list)
                # 读取下一行
                one_line = fd.readline()
    except Exception as e:
        print(str(e))

    # 返回数据
    return students


def read_file_two(path: str, infos: list):
    """
    读取文件，存储的格式为：[{},{},{},{}]
    :param path:
    :param infos:
    :return:
    """
    # 定义一个list集合
    students = []
    # 开始读取
    try:
        # 使用with打开文件
        with open(path, mode="r", encoding="GBK") as fd:
            # 读取第一行
            one_line = fd.readline()
            # 判断是否存在
            while one_line:
                # 处理读取的一行信息
                one_line_list = one_line.strip().split(",")
                # 定义一个临时的字典
                one_student_dict = {}
                # 开始循环
                for index, val in enumerate(one_line_list):
                    # 拼接字典
                    one_student_dict[infos[index]] = val
                # 附加到集合
                students.append(one_student_dict)
                # 读取下一行
                one_line = fd.readline()
    except Exception as e:
        print("读取文件出现异常，具体原因：" + str(e))
    # 返回
    return students


if __name__ == '__main__':
    # 定义文件的路径
    file_path = r"E:\Python\Project\BasciProject\student.csv"
    # 提供学生信息
    info = ['sno', 'name', 'gender', 'birthday', 'mobile', 'email', 'address']
    # 读取文件
    student_list = read_file_two(file_path, info)
    # 打印
    print(student_list)
